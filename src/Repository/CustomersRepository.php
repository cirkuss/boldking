<?php

namespace App\Repository;

use App\Entity\Customer;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Customer|null find($id, $lockMode = null, $lockVersion = null)
 * @method Customer|null findOneBy(array $criteria, array $orderBy = null)
 * @method Customer[]    findAll()
 * @method Customer[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CustomersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Customer::class);
    }

    /**
     * @return Customer[] Returns an array of Customer objects
     * @throws \Exception
     */
    public function getAllHavingMoreThanOnePaidOrder()
    {
        return $this->createQueryBuilder('c')
            ->join('c.orders', 'o')
            ->where('o.status = :statusOrder')
            ->having('COUNT(o.id) > 1')
            ->orderBy('o.paidDate', 'DESC')
            ->setParameter('statusOrder', 'paid')
            ->groupBy('c.id')
            ->getQuery()
            ->getResult();
    }

    /**
     * @return Customer[] Returns an array of Customer objects
     * @throws \Exception
     */
    public function getAllHavingActiveSubscriptionOnePaidOrder()
    {
        return $this->createQueryBuilder('c')
            ->join('c.orders', 'o')
            ->join('c.subscriptions', 's')
            ->where('o.status = :statusOrder')
            ->andWhere('s.active = 1')
            ->having('COUNT(o.id) >= 1')
            ->andHaving('COUNT(s.id) >= 1')
            ->orderBy('o.paidDate', 'DESC')
            ->setParameter('statusOrder', 'paid')
            ->groupBy('c.id')
            ->getQuery()
            ->getResult();
    }
}
