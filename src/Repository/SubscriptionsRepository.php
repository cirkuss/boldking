<?php

namespace App\Repository;

use App\Entity\Subscription;
use App\Helpers\DateTimeHelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Subscription|null find($id, $lockMode = null, $lockVersion = null)
 * @method Subscription|null findOneBy(array $criteria, array $orderBy = null)
 * @method Subscription[]    findAll()
 * @method Subscription[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SubscriptionsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Subscription::class);
    }

    /**
     * @return Subscription[] Returns an array of Subscription objects
     * @throws \Exception
     */
    public function findToUpdate()
    {
        return $this->createQueryBuilder('s')
            ->where('s.active = 1')
            ->andWhere('s.nextOrderDate < :now')
            ->setParameter('now', DateTimeHelper::getNow())
            ->getQuery()
            ->getResult();
    }
}
