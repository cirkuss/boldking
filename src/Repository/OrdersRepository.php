<?php

namespace App\Repository;

use App\Entity\Order;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdersRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Order::class);
    }

    /**
     * @return Order[] Returns an array of Customer objects
     * @throws \Exception
     */
    public function getLastOrdersByCustomer()
    {
        return $this->createQueryBuilder('o')
            ->join('o.customer', 'c')
            ->where('o.status != :statusOrder')
            ->orderBy('o.paidDate', 'DESC')
            ->setParameter('statusOrder', 'paid')
            ->groupBy('c.id')
            ->getQuery()
            ->getResult();
    }
}
