<?php

namespace App\Controller;

use App\Entity\Customer;
use App\Entity\Delivery;
use App\Entity\Order;
use App\Entity\Subscription;
use App\Helpers\CsvHelper;
use JMS\Serializer\SerializerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/api", name="api_")
 */
class ApiController extends AbstractController
{
     /**
     * @Route("/subscriptions/{id}", name="set_subscription_iteration", methods={"PATCH"})
     * @ParamConverter("subscription", class="App\Entity\Subscription")
     * @param Request $request
     * @param Subscription $subscription
     * @return JsonResponse
     */
    public function setSubscriptionIteration(Request $request, Subscription $subscription)
    {
        if (($dayIteration = $request->get('dayIteration')) != null) {
            $entityManager = $this->getDoctrine()->getManager();
            $subscription->setDayIteration($dayIteration);
            $entityManager->flush();
            return new JsonResponse(['status' => true]);
        }
        return new JsonResponse(['status' => false]);
    }

    /**
     * @Route("/customers/last_paid_order_date", name="customers_last_paid_order_date", methods={"GET"})
     * @param SerializerInterface $serializer
     * @return JsonResponse
     */
    public function getCustomersLastPaidOrderDate(SerializerInterface $serializer)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $customers = $entityManager->getRepository(Customer::class)->findAll();
        $response = [];
        foreach ($customers as $customer) {
            $response[$customer->getId()]['customer'] = $customer;

            /** @var Order $lastOrderPaid */
            $lastOrderPaid = $customer->getOrders()->filter(function (Order $entry) {
                return $entry->getStatus() === 'paid';
            })->first();
            $response[$customer->getId()]['last_paid_order_date'] = $lastOrderPaid instanceof Order ? $lastOrderPaid->getPaidDate()->format('Y-m-d') : '';
        }

        $json = $serializer->serialize($response, 'json');
        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/customers/more_than_one_paid_order", name="customers_more_than_one_paid_order", methods={"GET"})
     * @param SerializerInterface $serializer
     * @return JsonResponse
     * @throws \Exception
     */
    public function getCustomersMoreThanOnePaidOrder(SerializerInterface $serializer)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $customers = $entityManager->getRepository(Customer::class)->getAllHavingMoreThanOnePaidOrder();
        $json = $serializer->serialize($customers, 'json');
        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/customers/active_subscription_one_paid_order", name="customers_active_subscription_one_paid_order", methods={"GET"})
     * @param SerializerInterface $serializer
     * @return JsonResponse
     * @throws \Exception
     */
    public function getCustomersActiveSubscriptionOnePaidOrder(SerializerInterface $serializer)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $customers = $entityManager->getRepository(Customer::class)->getAllHavingActiveSubscriptionOnePaidOrder();
        $json = $serializer->serialize($customers, 'json');
        return new JsonResponse($json, 200, [], true);
    }

    /**
     * @Route("/deliveries/export", name="deliveries", methods={"GET"})
     * @return Response
     * @throws \Exception
     */
    public function getDeliveries()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $deliveries = $entityManager->getRepository(Delivery::class)->findAll();
        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="deliveries.csv"');
        $response->setContent(CsvHelper::makeDeliveriesExport($deliveries));
        return $response;
    }
}