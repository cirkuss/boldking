<?php

namespace App\EventListener;

use App\Entity\Order;
use App\Service\DeliveryService;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;

class OrderStatusUpdate
{
    protected $deliveryService;

    /**
     * OrderStatusUpdateSubscriber constructor.
     * @param DeliveryService $deliveryService
     */
    public function __construct(DeliveryService $deliveryService)
    {
        $this->deliveryService = $deliveryService;
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if ($entity instanceof Order && $entity->getStatus() === 'paid') {
            $this->deliveryService->createNewDelivery($entity);
        }
    }
}