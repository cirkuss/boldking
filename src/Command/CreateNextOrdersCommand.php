<?php

namespace App\Command;

use App\Service\SubscriptionService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateNextOrdersCommand extends Command
{
    use LockableTrait;

    protected static $defaultName = 'boldking:create-next-orders';

    private $subscriptionService;

    public function __construct(SubscriptionService $subscriptionService)
    {
        $this->subscriptionService = $subscriptionService;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Create next orders for active subscriptions');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        $totalOrdersCreated = $this->subscriptionService->createNextOrders();

        $output->writeln('New orders created: ' . $totalOrdersCreated);
        $this->release();

        $command = $this->getApplication()->find('boldking:update-next-orders');
        $command->run($input, $output);
    }
}