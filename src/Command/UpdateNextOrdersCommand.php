<?php

namespace App\Command;

use App\Service\SubscriptionService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Command\LockableTrait;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class UpdateNextOrdersCommand extends Command
{
    use LockableTrait;

    protected static $defaultName = 'boldking:update-next-orders';

    private $subscriptionService;

    public function __construct(SubscriptionService $subscriptionService)
    {
        $this->subscriptionService = $subscriptionService;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Update next order date for active subscriptions');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if (!$this->lock()) {
            $output->writeln('The command is already running in another process.');
            return 0;
        }

        $totalSubscriptionsUpdated = $this->subscriptionService->updateNextOrders();

        $output->writeln('Subscriptions updated: ' . $totalSubscriptionsUpdated);
        $this->release();
    }
}