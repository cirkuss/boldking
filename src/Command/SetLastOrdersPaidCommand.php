<?php

namespace App\Command;

use App\Service\OrderService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetLastOrdersPaidCommand extends Command
{
    protected static $defaultName = 'boldking-test:set-orders-to-paid';

    private $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;

        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Used for quick testing, set last orders created to paid in order to generate deliveries');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->orderService->setLastOrdersPaid();
    }
}