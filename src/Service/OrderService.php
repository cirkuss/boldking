<?php

namespace App\Service;

use App\Entity\Delivery;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;

class OrderService
{
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function setLastOrdersPaid()
    {
        $lastOrders = $this->entityManager->getRepository(Order::class)->getLastOrdersByCustomer();
        foreach ($lastOrders as $order) {
            $order->setStatus('paid');
            $this->entityManager->flush($order);
        }

    }
}