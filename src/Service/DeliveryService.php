<?php

namespace App\Service;

use App\Entity\Delivery;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;

class DeliveryService
{
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function createNewDelivery(Order $order)
    {
        $delivery = new Delivery();
        $delivery->setOrder($order);
        foreach ($order->getSubscription()->getItems() as $item) {
            $delivery->addItem($item);
        }
        $order->setDelivery($delivery);
        $this->entityManager->persist($delivery);
        $this->entityManager->flush($delivery);
    }
}