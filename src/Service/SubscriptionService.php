<?php

namespace App\Service;

use App\Entity\Order;
use App\Entity\Subscription;
use App\Helpers\DateTimeHelper;
use Doctrine\ORM\EntityManagerInterface;

class SubscriptionService
{
    protected $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    private function getTotalOrder(Subscription $subscription)
    {
        $total = 0;
        foreach ($subscription->getItems() as $item)
        {
            $total += $item->getPrice();
        }
        return $total;
    }

    public function createNextOrders(): int
    {
        $subscriptionsNeedingOrders = $this->entityManager->getRepository(Subscription::class)->findBy(['nextOrderDate' => DateTimeHelper::getNow(), 'active' => true]);
        $totalOrdersCreated = 0;
        /** @var Subscription $subscription */
        foreach ($subscriptionsNeedingOrders as $subscription) {
            $order = new Order();
            $order->setCustomer($subscription->getCustomer());
            $order->setStatus('created');
            $order->setPaidDate(DateTimeHelper::getNow());
            $order->setSubscription($subscription);
            $order->setTotal($this->getTotalOrder($subscription));
            $this->entityManager->persist($order);
            $totalOrdersCreated++;
        }
        $this->entityManager->flush();
        return $totalOrdersCreated;
    }

    public function updateNextOrders(): int
    {
        $subscriptionsNeedingOrders = $this->entityManager->getRepository(Subscription::class)->findToUpdate();
        $totalSubscriptionsUpdated = 0;
        /** @var Subscription $subscription */
        foreach ($subscriptionsNeedingOrders as $subscription) {
            do {
                $newDate = $subscription->getNextOrderDate();
                $subscription->setNextOrderDate(clone $newDate->modify('+' . $subscription->getDayIteration() . ' days'));
            } while ($newDate <= DateTimeHelper::getNow());
            $totalSubscriptionsUpdated++;
        }
        $this->entityManager->flush();
        return $totalSubscriptionsUpdated;
    }
}