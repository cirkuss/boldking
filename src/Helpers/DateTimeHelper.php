<?php

namespace App\Helpers;

class DateTimeHelper
{
    public static function getNow()
    {
        return (new \DateTime())->setTime(0,0,0, 0);
    }
}