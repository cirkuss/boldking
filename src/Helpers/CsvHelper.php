<?php

namespace App\Helpers;

use App\Entity\Delivery;
use App\Entity\Item;

class CsvHelper
{
    public static function makeDeliveriesExport(array $deliveries)
    {
        $formattedAnswer = 'id, order_id, items' . PHP_EOL;
        /** @var Delivery $delivery */
        foreach ($deliveries as $delivery)
        {
            $nbItem = 1;
            $items = '';
            $maxItem = count($delivery->getItems());
            /** @var Item $item */
            foreach ($delivery->getItems() as $item) {
                $items .= $item->getName() . ($nbItem != $maxItem ? ' - ' : '');
                $nbItem++;
            }
            $formattedAnswer .= $delivery->getId() . ',' . $delivery->getOrder()->getId() . ',' . $items . PHP_EOL;
        }
        return $formattedAnswer;
    }
}