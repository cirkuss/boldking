<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190219200143 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE Customers (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, active TINYINT(1) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Deliveries (id INT AUTO_INCREMENT NOT NULL, order_id INT DEFAULT NULL, UNIQUE INDEX UNIQ_205A86B88D9F6D38 (order_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE delivery_item (delivery_id INT NOT NULL, item_id INT NOT NULL, INDEX IDX_CE87ED8412136921 (delivery_id), INDEX IDX_CE87ED84126F525E (item_id), PRIMARY KEY(delivery_id, item_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Items (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, price INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Orders (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, subscription_id INT DEFAULT NULL, delivery_id INT DEFAULT NULL, status VARCHAR(50) NOT NULL, total INT NOT NULL, paid_date DATETIME NOT NULL, INDEX IDX_E283F8D89395C3F3 (customer_id), INDEX IDX_E283F8D89A1887DC (subscription_id), UNIQUE INDEX UNIQ_E283F8D812136921 (delivery_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE Subscriptions (id INT AUTO_INCREMENT NOT NULL, customer_id INT DEFAULT NULL, start_date DATETIME NOT NULL, nextorder_date DATETIME NOT NULL, day_iteration INT NOT NULL, active TINYINT(1) NOT NULL, INDEX IDX_B709C1F49395C3F3 (customer_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subscription_item (subscription_id INT NOT NULL, item_id INT NOT NULL, INDEX IDX_282735009A1887DC (subscription_id), INDEX IDX_28273500126F525E (item_id), PRIMARY KEY(subscription_id, item_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE Deliveries ADD CONSTRAINT FK_205A86B88D9F6D38 FOREIGN KEY (order_id) REFERENCES Orders (id)');
        $this->addSql('ALTER TABLE delivery_item ADD CONSTRAINT FK_CE87ED8412136921 FOREIGN KEY (delivery_id) REFERENCES Deliveries (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE delivery_item ADD CONSTRAINT FK_CE87ED84126F525E FOREIGN KEY (item_id) REFERENCES Items (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE Orders ADD CONSTRAINT FK_E283F8D89395C3F3 FOREIGN KEY (customer_id) REFERENCES Customers (id)');
        $this->addSql('ALTER TABLE Orders ADD CONSTRAINT FK_E283F8D89A1887DC FOREIGN KEY (subscription_id) REFERENCES Subscriptions (id)');
        $this->addSql('ALTER TABLE Orders ADD CONSTRAINT FK_E283F8D812136921 FOREIGN KEY (delivery_id) REFERENCES Deliveries (id)');
        $this->addSql('ALTER TABLE Subscriptions ADD CONSTRAINT FK_B709C1F49395C3F3 FOREIGN KEY (customer_id) REFERENCES Customers (id)');
        $this->addSql('ALTER TABLE subscription_item ADD CONSTRAINT FK_282735009A1887DC FOREIGN KEY (subscription_id) REFERENCES Subscriptions (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE subscription_item ADD CONSTRAINT FK_28273500126F525E FOREIGN KEY (item_id) REFERENCES Items (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE Orders DROP FOREIGN KEY FK_E283F8D89395C3F3');
        $this->addSql('ALTER TABLE Subscriptions DROP FOREIGN KEY FK_B709C1F49395C3F3');
        $this->addSql('ALTER TABLE delivery_item DROP FOREIGN KEY FK_CE87ED8412136921');
        $this->addSql('ALTER TABLE Orders DROP FOREIGN KEY FK_E283F8D812136921');
        $this->addSql('ALTER TABLE delivery_item DROP FOREIGN KEY FK_CE87ED84126F525E');
        $this->addSql('ALTER TABLE subscription_item DROP FOREIGN KEY FK_28273500126F525E');
        $this->addSql('ALTER TABLE Deliveries DROP FOREIGN KEY FK_205A86B88D9F6D38');
        $this->addSql('ALTER TABLE Orders DROP FOREIGN KEY FK_E283F8D89A1887DC');
        $this->addSql('ALTER TABLE subscription_item DROP FOREIGN KEY FK_282735009A1887DC');
        $this->addSql('DROP TABLE Customers');
        $this->addSql('DROP TABLE Deliveries');
        $this->addSql('DROP TABLE delivery_item');
        $this->addSql('DROP TABLE Items');
        $this->addSql('DROP TABLE Orders');
        $this->addSql('DROP TABLE Subscriptions');
        $this->addSql('DROP TABLE subscription_item');
    }
}
