<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190219200144 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('INSERT INTO Customers (id, `name`, email, active) VALUES 
                                                                        (1, "John Doe", "john@boldking.com", 1),
                                                                        (2, "Mary Jane", "mary@boldking.com", 1),
                                                                        (3, "Yuri Ali", "yuri@boldking.com", 1),
                                                                        (4, "Empty", "empty@boldking.com", 1);');
        $this->addSql('INSERT INTO Subscriptions (id, customer_id, start_date, nextorder_date, day_iteration, active) VALUES 
                                                                                                      (1, 1, "2011-03-01", "2017-06-01", 60, 1),
                                                                                                      (2, 2, "2016-01-01", "2017-01-01", 30, 1),
                                                                                                      (3, 3, "2017-04-01", "2017-07-21", 20, 1),
                                                                                                      (4, 4, "2017-04-01", "2017-07-21", 20, 0);');
        $this->addSql('INSERT INTO Orders (id, customer_id, subscription_id, `status`, total, paid_date) VALUES 
                                                                                         (1, 1, NULL, "paid", 50, "2016-01-05"),
                                                                                         (2, 1, NULL, "paid", 50, "2016-10-05"),
                                                                                         (3, 2, 2, "failed", 40, "2017-03-11"),
                                                                                         (4, 3, 3, "paid", 100, "2017-06-01"),
                                                                                         (5, 3, 3, "paid", 100, "2017-07-01"),
                                                                                         (6, 2, 2, "created", 10, "2016-04-22"),
                                                                                         (7, 2, 2, "paid", 20, "2017-06-01");');
        $this->addSql('INSERT INTO Items (id, name, price) VALUES 
                                           (1, "Face cream", 14),
                                           (2, "Refill blades", 8),
                                           (3, "Deodorant (stick)", 11);');
        $this->addSql('INSERT INTO subscription_item (subscription_id, item_id) VALUES 
                                                                (1, 1),
                                                                (1, 2),
                                                                (1, 3),
                                                                (2, 1),
                                                                (2, 2),
                                                                (2, 3),
                                                                (3, 1),
                                                                (3, 2),
                                                                (3, 3);');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('TRUNCATE Customers');
        $this->addSql('TRUNCATE Orders');
        $this->addSql('TRUNCATE Subscriptions');
    }
}
