## Installation
1. create a simple virtualhost pointing on the public directory, htaccess in project will make the rest.
2. copy paste .env in .env.local and update the DATABASE_URL
3. run ```sh install.sh```

---

## Tasks

1.  Create an order based on subscription next order date 
=> I've created a console command which can be executed typing:
```
php bin/console boldking:create-next-orders
```
==> It will create new order when 'today' is the day of nextorder_date
==> This command is calling `boldking:update-next-orders` when it's finished.

2.  Set customer’s next subscription (next order date) based on customer’s day iteration
=> I've created a console command which can be executed typing: 
```
php bin/console boldking:update-next-orders
```

3. Set customer's iteration frequency
=> the customer iteration subscription can be updated calling the following api endpoint:
```
method PATCH: /api/subscriptions/{idSubscription}
``` 
with param dayIteration

4. Get customers last paid order date
=> the list of customers can be get calling the following api endpoint:
```
method GET: /api/customers/last_paid_order_date
```

5. Get all customers with more than one paid order
=> the list of customers can be get calling the following api endpoint:
```
method GET: /api/customers/more_than_one_paid_order
```
 
6. Get all customers with an active subscription and with at least one paid order
=> the list of customers can be get calling the following api endpoint:
```
method GET: /api/customers/active_subscription_one_paid_order
``` 

Optional:
=> in order to make it easier to show that it's creating deliveries when an order is set to paid, I've created a console command:
```
php bin/console boldking-test:set-orders-to-paid
```
=> export csv:
```
method GET: /api/deliveries/export
```

---

## Technical choices
### Framework: Symfony
Reason: it's the one i'm the more comfortable with and which would made everything wanted.

### Console commands
Reason: it's mostly because it's things that need to be done once a day and could be executed by a cron.
